import 'npm://node-telegram-bot-api@0.52.0##node-telegram-bot-api'
import Telegram from 'node-telegram-bot-api'

import {BotChat} from './bot.chat.ts'

//import {init} from './db/init'
//import {MiPago, MiPagoOperator,MiPagoPackage} from './MiPago'

import fs from 'gh+/kwruntime/std@1.1.0/fs/mod.ts'
import Path from 'path'


process.env.NTBA_FIX_319 = "1"
process.env.NTBA_FIX_350 = "1"


export var kawixDynamic = {
    time: 150000
}

export class Bot{
    bot: Telegram 
    chats: {[key:string]: BotChat} = {}
    mongo: any 
    plugins = []
    config:any 
 
    $workingDir: string 


    constructor(workingDir: string){
        this.$workingDir = workingDir
    }
    

    async start(){
        let opt = { polling: true }
        
        let data = await kawix.import(this.$workingDir + "/config")
        this.config = data
        this.bot = new Telegram(data.token, opt)
        this.bot.on("polling_error", console.log);
        this.bot.on("message", (msg) => this.processMessage(msg))
        this.bot.on("callback_query", (q)=>{

            
            let msg = q.message
            let chat = this.getBotChat(msg)
            let parts = q.data.split(">")
            let param = parts.slice(1).join("/")
            msg.queryData = param
            chat.processMessage(msg, parts[0], true)


        })
        await this.readPlugins()
        // refresh plugins each 60 seconds
        setInterval(this.readPlugins.bind(this), 30000)
    }
    

    async readPlugins() {

        let _plugins = []

        // read the directory 
        let dirs = await fs.readdirAsync(Path.join(this.$workingDir))
        for (let i = 0; i < dirs.length; i++) {
            let file = Path.join(this.$workingDir, dirs[i])
            //console.info("file:",file)
            try {
                let stat = await fs.statAsync(file)
                if (stat.isDirectory()) {
                    let plugin = Path.join(file, "bot.plugin")
                    let exts = [".ts", ".js"]
                    for (let ext of exts) {
                        if (fs.existsSync(plugin + ext)) {
                            console.info("Reading plugin:", plugin)
                            let stat = await fs.statAsync(plugin + ext)
                            if (stat.isFile()) {
                                let mod = await kawix.import(plugin)
                                _plugins.push(mod.Plugin)
                                if(mod.start) await mod.start(this)
                            }
                        }
                    }
                }
            }
            catch (e) {
                console.error("Failed reading: ", file, e.message)
            }
        }

        _plugins = _plugins.filter((a) => !!a)
        for(let plugin of _plugins){
            if(plugin.$loadedTime === undefined)
                plugin.$loadedTime = Date.now()
        }
        this.plugins = _plugins

        for(let id in this.chats){
            this.chats[id].plugins = this.plugins
        }

    }

    

    processMessage(msg:Telegram.Message){
        
        let chat = this.getBotChat(msg)
        chat.processMessage(msg)
        
    }

    getBotChat(msg: Telegram.Message){
        if(!this.chats[msg.chat.id]){
            this.chats[msg.chat.id] = new BotChat(this, msg.chat)
            this.chats[msg.chat.id].plugins = this.plugins
        }
        return this.chats[msg.chat.id]
    }

}



