import {Bot} from './bot.ts'



main()

async function main(){

    // read parameters 
    let params:any = {}
	for(let i=1;i<kawix.appArguments.length;i++){
		let arg = kawix.appArguments[i]
		let parts = arg.split("=")
		let name = parts[0].substring(2)

		let value = parts.slice(1).join("=") || ''
		params[name] = value
		params[name+"_Array"] = params[name+"_Array"] || []
		params[name+"_Array"].push(value)
	}

    console.info("Working dir:", params.path)
    let bot = new Bot(params.path) 
    await bot.start()
}