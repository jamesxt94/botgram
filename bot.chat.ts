import 'npm://node-telegram-bot-api@0.50.0##node-telegram-bot-api'
import Telegram from 'node-telegram-bot-api'

import {Bot} from './bot.ts'

export var kawixDynamic = {
    time: 150000
}

export class BotChat {
    main: Bot
    bot: Telegram
    chat: Telegram.Chat

    serviceName: string = 'default'
    serviceId: string = 'default'
    serviceDescription:string = ''
    $loadedTime = -1 

    current = []
    params = []
    _plugins = []
    pluginObjects: {[key:string]: BotChat} = {}

    
    constructor(bot, chat) {
        this.main = bot
        this.bot = bot.bot
        this.chat = chat
    }


    get plugins(){
        return Object.values(this.pluginObjects)
    }

    set plugins(plugins){
        this._plugins = plugins
        let plug = this.pluginObjects
        this.pluginObjects = {}
        for(let id in this.pluginObjects){
            this.pluginObjects[id].closed = true 
        }
        for(let i=0;i<this._plugins.length;i++){
            let plugin = this._plugins[i]
            let pluginc = plug[plugin.serviceId]
            
            if(!pluginc || (pluginc.$loadedTime != plugin.$loadedTime)){
                pluginc = new plugin(this)
                pluginc.$loadedTime = plugin.$loadedTime
            }

            pluginc.closed = false 
            this.pluginObjects[plugin.serviceId] = pluginc
        }
        
    }

    
    async processMessage(msg: Telegram.Message, text : string = null, fromQuery = false) {
        //console.info("Message:", msg)

        if(text === null){
            text = msg.text = msg.text || msg.caption || ""
        }
        let action = text.split(/\s+/g)[0]
        if(!fromQuery){
            if (action[0] != "/") {
                action = ""
            }
            else {
                action = action.substring(1)
            }
        }

        
        action = action.split("__")[0].split(" ")[0]
        for (let plugin of this.plugins) {
            await plugin.init()
        }
        let callback = this.getMethod(action, fromQuery)
        if(callback)
            await callback.call(this, msg)
    }

    getMethod(action:string, fromQuery = false){

        let original, absoluteOriginal
        absoluteOriginal = action 
        if (this.main.config.translate) {
            action = this.main.config.translate[action] || action
        }
        original = action
        


        if (this.current.length) {
            let actions = [].concat(this.current)
            if (action)
                actions.push(action)
            action = actions.join("_")
        }
        console.info(" -> Processing action:", action)


        let actionFunc = null , reset = false
        for(let plugin of this.plugins){
            if(plugin[action]){
                actionFunc = plugin[action].bind(plugin)
                break 
            }
        }
        if (!actionFunc) {
            actionFunc = this[action]
        }
        if (!actionFunc && (original != action)) {

            console.info(" -> Processing action:", original)
            for (let plugin of this.plugins) {
                if (plugin[original]) {
                    actionFunc = plugin[original].bind(plugin)
                    break
                }
            }
            if (!actionFunc) {
                actionFunc = this[original]
            }
            if(actionFunc) reset = true 
        }

        if(!fromQuery){
            if(typeof actionFunc != "function"){
                console.info(" -> Fallback to help")
                if (absoluteOriginal == "help") 
                    actionFunc= this.help 
                else 
                    actionFunc = this.getMethod("help")
            }
            if(reset) this.reset()
        }
        return actionFunc
    }

    init(){}
    cancelar(msg) {
        this.reset()
        this.getMethod("help").call(this, msg)
    }

    reset() {
        this.current = []
        this.params = []
    }

    resetHistory() {
        this.current = []
    }

    help(msg: Telegram.Message) {

        let text = []
        
        for(let plugin of this.plugins){
            text.push(` /${plugin.serviceId}    ${plugin.serviceDescription}`)
        }

        this.bot.sendMessage(msg.chat.id, `Bienvenido a @kodhe\\_bot.
        
Este bot es multipropósito, se irán añadiendo más funciones con el tiempo. Algunas funciones necesitan previo registro y autorización de nuestra parte.
Ofrecemos servicios informáticos:
    - Programación de Software
    - Bots telegram/whatsapp
    - Scrapping
    - Administración de servidores
    Entre otros

Para más información escriba a @kodhe\\_work

*Funciones del bot disponibles:*
${text.join("\n")}
`, {
            parse_mode: 'Markdown'
        })
    }



}